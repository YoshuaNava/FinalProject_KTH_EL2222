#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 26 23:28:09 2017

@author: alfredoso
"""


import rospy
from std_msgs.msg import Int16MultiArray
from matplotlib import pyplot as plt
import math
import numpy as np


prev_time = 0

wheel_radius = 3
distance_wheels = 17.2
line_thresh = 800
line_sensors_dist = 0.95

wF_pos = np.zeros( (2,1), dtype=np.float64)
robot_theta = 0
wF_R = np.zeros( (2,2) )

fig, ax = plt.subplots()
plt.xlim([-100, 100])
plt.ylim([-100, 100])


#def update_plot(line_sensors)

def calculate_linear_velocities(wheel_odometry_val):
    global robot_theta, wheel_radius, wF_R
    v_left = wheel_odometry_val[0] * wheel_radius
    v_right = wheel_odometry_val[1] * wheel_radius
    
    rF_v_x = (v_left + v_right) / 2
    rF_v = np.array( [[rF_v_x], [0]] )
    
    theta_dot = (v_right - v_left) / distance_wheels
    
    wF_R = np.matrix( [ [math.cos(robot_theta), -math.sin(robot_theta)], [math.sin(robot_theta), math.cos(robot_theta)]] )
    
    wF_v = np.array(wF_R * rF_v)
    
#    print("v_left", v_left)
#    print("v_right", v_right)
#    print("rF_v", rF_v)
#    print("wF_R", wF_R)
#    print("theta_dot", theta_dot)
#    print("wF_v", wF_v)

    return wF_v, theta_dot
    
    
    
def integrate_odometry(wheel_odometry_val, dt):
    global wF_pos, robot_theta, wF_R, ax
    
    wF_v, theta_dot = calculate_linear_velocities(wheel_odometry_val)
    wF_pos = wF_pos + dt * wF_v
    robot_theta = robot_theta + dt * theta_dot
    robot_theta = math.atan2( math.sin(robot_theta), math.cos(robot_theta) )
    
    print("robot position", wF_pos)
    print("robot theta", robot_theta)    



def plot_everything(line_sensors_val):
    global wF_pos, wF_R    
    ax.plot(wF_pos[0], wF_pos[1], c='m', marker='*')
    
    line_pos = np.zeros( (2,1) )
    sensors_active = 0
    
    for i in range(8):
        if line_sensors_val[i] >= line_thresh:
            
            print("line sensor value", line_sensors_val[i])
            rF_pos_sensor = np.array( [[0], [line_sensors_dist * (4-i)]])
            wF_pos_sensor = wF_pos + wF_R * rF_pos_sensor
            print("rF_pos_sensor", rF_pos_sensor)
            print("wF_pos_sensor", wF_pos_sensor)
            line_pos = line_pos + wF_pos_sensor
            sensors_active += 1
            ax.plot(wF_pos_sensor[0], wF_pos_sensor[1], c='g', marker='.')
    if(sensors_active > 0):
        line_pos = line_pos / sensors_active
        ax.plot(line_pos[0], line_pos[1], c='k', marker='s')
    plt.show(block=True)



def callback(data):
    global prev_time
    rospy.loginfo("I heard %s", data.data)
    
    line_sensors_val = np.array( data.data[0:8], dtype=np.float64 )
    wheel_odometry_val = np.array( data.data[8:10], dtype=np.float64 )
    
    dt = rospy.get_time() - prev_time
    prev_time = rospy.get_time()
    
    integrate_odometry(wheel_odometry_val, dt)
    
    plot_everything(line_sensors_val)



def listener():
    global prev_time
    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)
    
    prev_time = rospy.get_time()
    rospy.Subscriber("sensors", Int16MultiArray, callback)
    plt.show(block=True)
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == '__main__':
    listener()