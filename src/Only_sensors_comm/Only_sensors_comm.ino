#include "motors.h"
#include <ros.h>
#include <std_msgs/Int16.h>
#include <std_msgs/Int16MultiArray.h>
#include <std_msgs/Bool.h>



ros::NodeHandle nh;
std_msgs::Int16MultiArray sensors_msg;
ros::Publisher sensorsPublisher("sensors", &sensors_msg);
int sensors_state[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};



bool followLine = false;

void on_off_cb( const std_msgs::Bool& cmd_msg)
{
  if(cmd_msg.data == false)
  {
    followLine = false;
  }
  else
  {
    followLine = true;
  }
}

ros::Subscriber<std_msgs::Bool> sub("on_off", on_off_cb);

void setup()
{
	
	delay(10);
  
  nh.initNode();
  nh.getHardware()->setBaud(115200);
  nh.advertise(sensorsPublisher);
  nh.subscribe(sub);
  sensors_msg.layout.dim = (std_msgs::MultiArrayDimension *) malloc(sizeof(std_msgs::MultiArrayDimension) * 1);
  sensors_msg.data_length = 10;
  sensors_msg.data = sensors_state;    
}


void loop()
{
  for(int i=0; i<8 ;i++)
  {
    sensors_state[i] = random(0, 1000);
//    sensors_state[i] = 1000;
  }
  sensors_state[8] = 1;
  sensors_state[9] = 1;
  
  sensors_msg.data = sensors_state;
  sensorsPublisher.publish( &sensors_msg );
  nh.spinOnce();
  delay(50);
}


