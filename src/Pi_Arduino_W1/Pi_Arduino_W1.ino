#include "motors.h"
#include <QTRSensors.h>
#include <ros.h>
#include <std_msgs/Byte.h>
#include <std_msgs/Int16.h>
#include <std_msgs/Int16MultiArray.h>
#include <std_msgs/Bool.h>


// QTR8RC stuff (IR sensor array)
#define NUM_SENSORS   8     // number of sensors used
#define TIMEOUT       2500  // waits for 2500 microseconds for sensor outputs to go low
#define EMITTER_PIN   30     // emitter is controlled by digital pin 2

// sensors 0 through 7 are connected to digital pins 3 through 10, respectively
QTRSensorsRC qtrrc((unsigned char[]) {32, 34, 36, 38, 40, 44, 46, 48},
	NUM_SENSORS, TIMEOUT, EMITTER_PIN);
unsigned int sensorValues[NUM_SENSORS];
// Velocity and position variables
int position, V0, VL, VR, Vmin, Vmax, Vamp;
// Gain of the line-following control loop
float P;


//384 ticks per circle
const double DEG_PER_TIC = (double)360 / (double)384;
const double WHEEL_RADIUS = 0.06425 / 2;
const double ROBOT_BASE = 0.13;
unsigned int left_enc_count, right_enc_count;
unsigned long time_prev, time_now;
double left_angular_vel, right_angular_vel;
//Create motor objects with connections and  parameters
//Arguments: encoder pin, to motor : out1, out2, enable pin,inverse direction,Kp,Ki,Kd
Motor left_motor(3, 10, 11, 6, false, 4.0, 0, 2);
Motor right_motor(2, 8, 9, 5, true, 6.0, 0, 2.5);


ros::NodeHandle nh;
std_msgs::Int16MultiArray sensors_msg;
ros::Publisher sensorsPublisher("sensors", &sensors_msg);
int sensors_state[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};



bool followLine = false;
// false moves forward
bool direction = false;

void on_off_cb( const std_msgs::Bool& cmd_msg)
{
  if(cmd_msg.data == false)
  {
    followLine = false;
  }
  else
  {
    followLine = true;
  }
}

ros::Subscriber<std_msgs::Bool> sub("on_off", on_off_cb);

void setup()
{
  delay(500);
  
  //Configure interrupt pins for encoders
  attachInterrupt(digitalPinToInterrupt(left_motor.ENCODER_PIN), left_tic_counter , CHANGE);
  attachInterrupt(digitalPinToInterrupt(right_motor.ENCODER_PIN), right_tic_counter , CHANGE);
  
  // Specify lowest and highest PWM values that the motors function
  Vmin = 0;
  Vmax = 20;
  // Specify desired straight-line speed of the robot (PWM range: Vmin - 255)
  V0 = 10;
  // Find maximum permisible amplitude of velocity command (around V0)
  Vamp = V0 - Vmin;
  // Specify gain of line-following algorithm (P range: 0.0 - 1.0)
  P = 1;
  
  // Calibrate all sensors to light conditions
	for (int i = 0; i < 400; i++)  // make the calibration last about 10 seconds
	{
		qtrrc.calibrate();         // reads all sensors 10 times at 2500 us per read (i.e. ~25 ms per call)

//    if ((i%7)==0)
//    {
//      direction = !direction;  
//    }    
//    
//	  right_motor.rotate(V0-2,direction);
//    left_motor.rotate(V0-2,!direction);
	}
  
  right_motor.rotate(0,false);
  left_motor.rotate(0,false);
 
	
	delay(10);
  
  nh.initNode();
  nh.getHardware()->setBaud(115200);
  nh.advertise(sensorsPublisher);
  nh.subscribe(sub);
  sensors_msg.layout.dim = (std_msgs::MultiArrayDimension *) malloc(sizeof(std_msgs::MultiArrayDimension) * 1);
  sensors_msg.data_length = 10;
  sensors_msg.data = sensors_state;    
}


void loop()
{
  // Map position with respect to center of sensor array
  position = map(qtrrc.readLine(sensorValues), 0, 7000, -Vamp, Vamp);

  if(followLine == true)
  {
  	// Calculate differential drive speed of each wheel
  	VR = V0-(int)(P*position);
    VL = V0+(int)(P*position);
    
  }
  else
  {
    VR = 0;
    VL = 0;
  }
  
  // Send motor command
  right_motor.rotate(VR, false);
  left_motor.rotate(VL, false);
  
  for(int i=0; i<8 ;i++)
  {
    sensors_state[i] = sensorValues[i];
  }
  sensors_state[8] = left_motor.get_angular_vel();
  sensors_state[9] = right_motor.get_angular_vel();
  
  sensors_msg.data = sensors_state;
  sensorsPublisher.publish( &sensors_msg );
  nh.spinOnce();
  delay(50);
}


//Callback functions when interrupt are triggered by encoders
void left_tic_counter() 
{
  //call motor tick counter
  left_motor.update_ticks();
}



void right_tic_counter()
{
  //call motor tick counter
  right_motor.update_ticks();
}

