#include <PID_v1.h>




const double MICRO_PER_SEC = 1000000.0;
const double TICS_SAMPLING_TIME = 0.2;





//Motor Class with different paramters for configuring the connections
class Motor {
  private:
    bool INVERSE;
    double Kp, Ki, Kd;
    unsigned long time_prev, time_now;
    double angular_vel, time_diff, prev_err = 0, integ_err = 0;
    double TICS_PER_ROTATION = 384; //This can be varied based on interrupt configuration
    int current_pwm = 0;
    double correction_vel, desired_vel;
    PID pid;
    const double MAX_ANGULAR_VEL = 200;
    const double MIN_MOVING_PWM = 00;
    const double MAX_PWM = 230;
  public:
    byte ENCODER_PIN, OUT1, OUT2, ENB;
    unsigned int tic_count;
    Motor(byte enc_pin, byte out1, byte out2, byte enb, bool inverse,
          double Kp, double Ki, double Kd);
    void calc_angular_vel();
    void rotate(double ref, int direction);
    double get_angular_vel();
    double get_max_angular_vel();
    double get_max_pwm();
    double get_min_moving_pwm();
    void update_ticks();
    void pid_controller(double ref);
    void map_corrected_vel_pwm();
};


//Initialize the motor parameters
Motor::Motor(byte enc_pin, byte out1, byte out2, byte enb, bool inverse, double Kp, double Kd, double Ki)
  : pid(&angular_vel, &correction_vel, &desired_vel, Kp, Ki, Kd, DIRECT)
{
  this->ENCODER_PIN = enc_pin;
  this->OUT1 = out1;
  this->OUT2 = out2;
  this->ENB = enb;
  this->INVERSE = inverse;
  this->Kp = Kp;
  this->Kd = Kd;
  this->Ki = Ki;
  pid.SetSampleTime(TICS_SAMPLING_TIME);
  pid.SetMode(AUTOMATIC);
  pid.SetOutputLimits(-MAX_ANGULAR_VEL, MAX_ANGULAR_VEL);
  this->time_prev = micros();
  pinMode(enc_pin, INPUT_PULLUP);
  pinMode(out1, OUTPUT);
  pinMode(out2, OUTPUT);
  pinMode(enb, OUTPUT);
}



//function to rotate the motor, parameters: reference velocity and direction of motor
void Motor::rotate(double ref, int direction)
{
  INVERSE = direction;
  if (!direction)
  //forward
  {
    digitalWrite(OUT1, LOW);
    digitalWrite(OUT2, HIGH);
  }
  else
  //reverse
  {
    digitalWrite(OUT1, HIGH);
    digitalWrite(OUT2, LOW);
  }
  calc_angular_vel();
  desired_vel = ref;
    
  // Two different ways to compute the PID controller.
  // Using the PID Arduino library:
  pid.Compute();
  // Using a custom made PID algorithm:
  //pid_controller(ref);
  
  map_corrected_vel_pwm();
//  analogWrite(ENB, 0);
//  analogWrite(ENB, 230);

  analogWrite(ENB, current_pwm);

//  Serial.println(angular_vel);

  // Debugging block:
//  Serial.println("-------------------");
//  Serial.println(ENB);
//  Serial.println("Angular velocity = ");
//  Serial.println(angular_vel);
//  Serial.println("Desired velocity = ");
//  Serial.println(desired_vel);
//  Serial.println("Velocity correction = ");
//  Serial.println(correction_vel);
//  Serial.println("PWM value = ");
//  Serial.println(current_pwm);
}



int sign(double number)
{
  return (number > 0) - (number < 0);
}

// Alternative control algorithm to maintain the speed
void Motor::pid_controller(double ref)
{
  if (time_diff >= TICS_SAMPLING_TIME)
  {
    double p, i, d, error;
    error = ref - angular_vel;
    p = Kp * error;
    integ_err = integ_err + error;
    i = Ki * integ_err;
    d = Kd * (error - prev_err) / time_diff;
    prev_err = error;
    correction_vel = p + i + d;
    if (abs(correction_vel) > MAX_ANGULAR_VEL)
    {
      correction_vel = sign(correction_vel) * MAX_ANGULAR_VEL;
    }
    
    // Debugging block:
    //      Serial.println(p);
    //      Serial.println(i);
    //      Serial.println(d);
  }
}



//function to calculate angular velocity
void Motor::calc_angular_vel()
{
  time_now = micros();
  time_diff = (time_now - time_prev) / MICRO_PER_SEC;
  if (time_diff >= TICS_SAMPLING_TIME)
  {
    angular_vel = ((tic_count * 2 * PI) / TICS_PER_ROTATION) / TICS_SAMPLING_TIME;
    tic_count = 0;
    time_prev = time_now;
  }
}



//function to read the current angular velocity of motors
double Motor::get_angular_vel()
{
  return angular_vel;
}


double Motor::get_max_angular_vel()
{
  return MAX_ANGULAR_VEL;
}


double Motor::get_min_moving_pwm()
{
  return MIN_MOVING_PWM;
}


double Motor::get_max_pwm()
{
  return MAX_PWM;
}


//function to map the values of angular velocity to pwm
//***This part can be improved by using just an IF/ELSE instead of IF/IF/ELSE***
void Motor::map_corrected_vel_pwm()
{
  if (correction_vel <= 0)
  {
      current_pwm = 0;
  }
  else
  {
    if(desired_vel != 0)
    {
      current_pwm = map(correction_vel, 0, MAX_ANGULAR_VEL, MIN_MOVING_PWM, MAX_PWM);
    }
    else
    {
      current_pwm = 0;
    }
  }
}



//function to increment the tics for each motor.
void Motor::update_ticks()
{
  tic_count += 1;
}



