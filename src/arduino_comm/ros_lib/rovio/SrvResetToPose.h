#ifndef _ROS_SERVICE_SrvResetToPose_h
#define _ROS_SERVICE_SrvResetToPose_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Empty.h"
#include "geometry_msgs/Pose.h"

namespace rovio
{

static const char SRVRESETTOPOSE[] = "rovio/SrvResetToPose";

  class SrvResetToPoseRequest : public ros::Msg
  {
    public:
      typedef geometry_msgs::Pose _T_WM_type;
      _T_WM_type T_WM;

    SrvResetToPoseRequest():
      T_WM()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->T_WM.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->T_WM.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return SRVRESETTOPOSE; };
    const char * getMD5(){ return "1e9e36137d6a9824420bd1d5101fe972"; };

  };

  class SrvResetToPoseResponse : public ros::Msg
  {
    public:
      typedef std_msgs::Empty _nothing_type;
      _nothing_type nothing;

    SrvResetToPoseResponse():
      nothing()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->nothing.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->nothing.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return SRVRESETTOPOSE; };
    const char * getMD5(){ return "2a891d825193801ed1b6cde23b48a7ae"; };

  };

  class SrvResetToPose {
    public:
    typedef SrvResetToPoseRequest Request;
    typedef SrvResetToPoseResponse Response;
  };

}
#endif
