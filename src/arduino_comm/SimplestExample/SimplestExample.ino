#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Byte.h>
#include <std_msgs/Int16.h>
#include <std_msgs/Int16MultiArray.h>


ros::NodeHandle nh;

std_msgs::String str_msg;
ros::Publisher chatter("chatter", &str_msg);

std_msgs::Byte lineSensors_msg;
ros::Publisher lineSensorsPublisher("lineSensors", &lineSensors_msg);

std_msgs::Int16MultiArray wheelOdometry_msg;
ros::Publisher wheelOdometryPublisher("wheelOdometry", &wheelOdometry_msg);


char hello[13] = "hello world!";
byte line_sensors;
int wheel_odometry[2] = {0, 0};


void setup()
{
  nh.initNode();
  nh.getHardware()->setBaud(115200);
  
  nh.advertise(chatter);
  nh.advertise(lineSensorsPublisher);
  nh.advertise(wheelOdometryPublisher);

  wheelOdometry_msg.layout.dim = (std_msgs::MultiArrayDimension *) malloc(sizeof(std_msgs::MultiArrayDimension) * 1);
  wheelOdometry_msg.data_length = 2;
  wheelOdometry_msg.data = wheel_odometry;
}


void loop()
{
  str_msg.data = hello;
  chatter.publish( &str_msg );


  line_sensors = B00011000;
  lineSensors_msg.data = line_sensors;
  lineSensorsPublisher.publish( &lineSensors_msg );


  wheel_odometry[0] = 5;
  wheel_odometry[1] = 6;
  wheelOdometry_msg.data = wheel_odometry;
  wheelOdometryPublisher.publish( &wheelOdometry_msg );
  
  nh.spinOnce();
  delay(50);
}

